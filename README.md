# <img src="https://storage.googleapis.com/kaggle-avatars/images/4667463-kg.jpg" width = "100" height = "100" alt="https://www.kaggle.com/haoweihsu"/>Haowei's Portfolio of Kaggle

## Title

<img src="https://www.kaggle.com/static/images/tiers/contributor@96.png" width = "64" height = "64" alt="Contributor"/> Competitions Contributor

## Medals

| <img src="https://www.kaggle.com/static/images/medals/competitions/goldl@2x.png" width = "24" height = "20" alt="gold medal"/>Gold | <img src="https://www.kaggle.com/static/images/medals/datasets/silverl@2x.png" width = "24" height = "20" alt="silver medal"/>Silver | <img src="https://www.kaggle.com/static/images/medals/datasets/bronzel@2x.png" width = "24" height = "20" alt="broze medal"/>Bronze |
| ---------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------- |
| 0                                                                                                                                  | 0                                                                                                                                    | 0                                                                                                                                   |



## Bio

### Education and qualifications

- Received a Bachelor's degree of Information Management & Information System in Northwest A&F University.
- A Master Candidate in School of Artificial Intelligence, OPtics and ElectroNics (iOPEN) of Northwestern Polytechnical University.

### Honors and awards

- Mathematical Contest In Modeling / Interdisciplinary Contest In Modeling - Finalist

### Hobbies

- Modeling for Tabular Competitions
- Modeling for NLP

## Portfolio

### Getting Started

### Playground

### Featured
