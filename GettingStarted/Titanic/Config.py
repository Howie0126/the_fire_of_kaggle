from datetime import datetime

SETTINGS = {
    'ROOT_PATH': '../',

    'TRAIN_PATH': 'Input/Tables/train.csv',

    'TEST_PATH': 'Input/Tables/test.csv',

    'SUBMISSION_PATH': f"Output/Submissions/submission_{datetime.now().strftime('%Y-%m-%d')}.csv",

    'RANDOM_STATE': 42,

    'K-FOLDS': 5,

    'TEST_SIZE': 0.33,
}
